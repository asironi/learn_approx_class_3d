3D Low Rank Filters Approximation

Approximate an existing filter bank by learning another filter bank composed of separable filters using the method described in [1].

The algorithm's settings are in the helper_scripts_3D/get_config.m file.

+ Required libraries: 
-Teem/nrrd library ([2]). Once compiled for your
  architecture, copy them in the helper_scripts_3D/nrrd/ subdirectory.
- MATLAB Tensor Toolbox [3] and Poblano toolbox [4]. To use CP_OPT algorithm [5].

If you use this code in your project, please cite our paper ([1]).

For any question or bug report, please feel free to contact me at:
amos <dot> sironi <at> epfl <dot> ch

[1] R. Rigamonti, A. Sironi, V. Lepetit and P. Fua. Learning Separable Filters. IEEE International Conference on Computer Vision and Pattern Recognition (CVPR 2013), Portland, Oregon, USA, 2013.
[2] http://teem.sourceforge.net/nrrd/index.html
[3] http://www.sandia.gov/~tgkolda/TensorToolbox/
[4] https://software.sandia.gov/trac/poblano/
[5] E. Acar and D. M. Dunlavy and T. G. Kolda "A Scalable Optimization Approach for Fitting Canonical Tensor Decompositions". Journal of Chemometrics, 2011.

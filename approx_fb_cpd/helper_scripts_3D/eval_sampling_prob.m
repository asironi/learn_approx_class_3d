function [sampling_prob] = eval_sampling_prob(p,M,original_filters)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

sampling_prob = zeros(p.original_fb_no,1);

for i_x = 1 : p.original_fb_no
    x = original_filters(:,:,:,i_x);
    x = x(:);
    
    t = best_optimize_coeffs(p,M,x,1e-7);
    
    rec = M*t;
    sampling_prob(i_x) = norm(x-rec);
end

fprintf('    Average reconstruction error: %f\n',sum(sampling_prob)/p.original_fb_no);

end

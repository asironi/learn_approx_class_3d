function [img] = reshape_fb_as_img(p, M)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

n_blocks = p.filters_no;
n_block_rows = ceil(sqrt(n_blocks));
n_block_cols = ceil(sqrt(n_blocks));

img = 255*ones(n_block_rows*p.filters_size(1)*p.pixS + (n_block_rows-1)*p.vSpace, ...
               n_block_cols*p.filters_size(2)*p.pixS + (n_block_cols-1)*p.hSpace, ...
               p.filters_size(3)*p.pixS);

rowN = 1;
colN = 1;
for f = 1 : p.filters_no
    filter = reshape(M(:, f), p.filters_size);
    filter = magnify_matrix(filter, p.pixS); 
    filter = convert_img_visualization(filter);
    
    if(colN == n_block_cols+1)
        rowN = rowN+1;
        colN = 1;
    end
    
    img((rowN-1)*p.filters_size(1)*p.pixS + (rowN-1)*p.vSpace + 1 : rowN*p.filters_size(1)*p.pixS + (rowN-1)*p.vSpace, ...
        (colN-1)*p.filters_size(2)*p.pixS + (colN-1)*p.hSpace + 1 : colN*p.filters_size(2)*p.pixS + (colN-1)*p.hSpace, ...
        :) = filter;
    colN = colN+1;
end

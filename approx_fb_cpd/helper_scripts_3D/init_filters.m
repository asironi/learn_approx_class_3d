function [p,M] = init_filters(p, resume_fb_number)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

if(resume_fb_number == 0)
   
    %random filters entries
    %M = randn(p.filters_size^3,p.filters_no ); %valid only for square filters
    %uniform filter
    %M(:, 1) = ones(p.filters_size^3, 1);  %valid only for square filters
    
    %random filters entries
    M = randn(prod(p.filters_size),p.filters_no ); %any filter
    %uniform filter
    M(:, 1) = ones(prod(p.filters_size), 1);  %any filter
    
    
    %initialize random decomposition 
    for i_fil = 1: p.filters_no,
        %p.CPdec{i_fil}.U = {randn(p.filters_size,p.rank),randn(p.filters_size,p.rank),randn(p.filters_size,p.rank)}; %valid only with square filters
        p.CPdec{i_fil}.U = {randn(p.filters_size(1),p.rank),randn(p.filters_size(2),p.rank),randn(p.filters_size(3),p.rank)}; 
        p.CPdec{i_fil}.lambda = ones(1,p.rank);
    end
    
else

    %M = zeros(p.filters_size^3,p.filters_no);%%valid only for square filters
    M = zeros(prod(p.filters_size),p.filters_no);

    fb = load(fullfile(p.filters_txt_directory, sprintf('fb_%06d.txt', resume_fb_number)));
    %filter_size = size(fb, 2); %valid only for square filters
    for i_filter = 1 : p.filters_no
        filt = fb((i_filter-1)*p.filters_size(1)*p.filters_size(3)+1:i_filter*p.filters_size(1)*p.filters_size(3), :); 
        filt = permute(reshape(filt',[p.filters_size(2),p.filters_size(1),p.filters_size(3)]),[2,1,3]); 

        M(:, i_filter) = filt(:);

        CPInit = cp_opt(tensor(filt),p.rank);
        p.CPdec{i_filter}.U = CPInit.U;
        p.CPdec{i_filter}.lambda = CPInit.lambda;
        
    end
end

M = normalize_fb(p, M); 

end


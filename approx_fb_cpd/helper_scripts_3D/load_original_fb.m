function [p, original_filters] = load_original_fb(p)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

tmp_fb = load(p.original_filters_filename);


filters_size = p.filters_size; % any size of filters

if (filters_size(2) ~= size(tmp_fb,2))
    error('number of columns of original filter bank must be equal to p.filters_size(2)' );
end

filters_no = size(tmp_fb,1)/(filters_size(1)*filters_size(3));% N must be integer
if (filters_no~=round(filters_no))
    error('number of rows of original filter bank must be equal to p.filters_size(1)*p.filters_size(3)*filters_no' );

end

original_filters = zeros([filters_size, filters_no]);%


for i_filter = 1 : filters_no
    original_filter_temp = tmp_fb((i_filter-1)*filters_size(1)*filters_size(3)+1:i_filter*filters_size(1)*filters_size(3), :); 
    original_filters(:, :, :, i_filter) = permute(reshape(original_filter_temp',[filters_size(2),filters_size(1),filters_size(3)]),[2,1,3]); %any size filters
end

p.original_fb_no = filters_no;


end

function [] = save_reconstructed_filter_bank(p, M, count, original_filters)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

output_path = fullfile(p.reconstructed_filters_txt_directory, sprintf('fb_%06d.txt', count/p.steps_no)); %

rows_no = p.filters_size(1);
cols_no = p.filters_size(2);
sli_no = p.filters_size(3); 
avg_rec_error = 0;
avg_l0norm = 0;
avg_l1norm = 0;
rec_fb = zeros(rows_no*cols_no*sli_no,p.original_fb_no);
rec_t = zeros(p.filters_no,p.original_fb_no);
sampling_prob = zeros(p.original_fb_no,1);

    x = original_filters(:,:,:,1);
    x = x(:);
    
    t = best_optimize_coeffs(p,M,x,1e-7);
    
    rec = M*t;
    rec_fb(:,1) = rec;
    rec_t(:,1) = t(1:p.filters_no);
    sampling_prob(1) = norm(x-rec);
    avg_rec_error = avg_rec_error+norm(x-rec);
    sampling_prob(1) = norm(x-rec);
    avg_l0norm = avg_l0norm+sum(t~=0);
    avg_l1norm = avg_l1norm+sum(abs(t));
    rec = reshape(rec,p.filters_size); 
    
    dlmwrite(output_path, rec(:,:,1), 'delimiter', '\t', 'precision', 16);
    for i_sli = 2:p.filters_size(3),
        dlmwrite(output_path, rec(:,:,i_sli), 'delimiter', '\t', 'precision', 16, '-append'); 
    end

for i_x = 2 : p.original_fb_no
    x = original_filters(:,:,:,i_x); 
    x = x(:);
    
    t = best_optimize_coeffs(p,M,x,1e-7);
    
    rec = M*t;
    rec_fb(:,i_x) = rec;
    rec_t(:,i_x) = t(1:p.filters_no);
    sampling_prob(i_x) = norm(x-rec);
    avg_rec_error = avg_rec_error+norm(x-rec);
    sampling_prob(i_x) = norm(x-rec);
    avg_l0norm = avg_l0norm+sum(t~=0);
    avg_l1norm = avg_l1norm+sum(abs(t));
    rec = reshape(rec,p.filters_size); 
    
    
    for i_sli = 1:p.filters_size(3),
       dlmwrite(output_path, rec(:,:,i_sli), 'delimiter', '\t', 'precision', 16, '-append'); 
    end
    
end
avg_rec_error = avg_rec_error/(p.original_fb_no*rows_no*cols_no*sli_no);

%save coefficients
save(fullfile(p.coeffs_txt_directory, sprintf('coeffs_t_%06d.txt', count/p.steps_no)),'rec_t','-ascii','-double')
%save reconstructed fb image
nrrdSave(sprintf('%s/fb_%06d.nrrd', p.reconstructed_filters_img_directory, floor(count/p.steps_no)), permute(reshape_result_fb_as_img(p, rec_fb, p.original_fb_no),[2,1,3]));
%save statistics
fd_stats = fopen(fullfile(p.stats_directory, sprintf('stats_%06d.txt', count/p.steps_no)), 'wt');
fprintf(fd_stats,'Average error on original_fb     : %f\n',avg_rec_error);
fprintf(fd_stats,'Average l0 norm of original_fb   : %f\n',avg_l0norm/p.original_fb_no);
fprintf(fd_stats,'Average l1 norm of original_fb   : %f\n',avg_l1norm/p.original_fb_no);
%save ktensor coefficients
for i_fil = 1 : p.filters_no,
    fprintf(fd_stats,'Kruskal tensor weights lambda of filter %i : ',i_fil); 
    decompArranged = arrange(p.CPdec{i_fil});
    fprintf(fd_stats,'%f ',decompArranged.lambda');
    fprintf(fd_stats,'\n');
end 
fprintf(fd_stats,'Sampling probabilities:\n');
fprintf(fd_stats,'%f ',sampling_prob);
fprintf(fd_stats,'\n');
fclose(fd_stats);

         
fclose('all'); % nrrdSave gives error when saves too many images
         
end

function [p] = get_config(resume_fb)
%  get_config  setup the configuration for low rank 3D filter approximation
%
%  Synopsis:
%     [p] = get_config()
%
%  Ouput:
%     p = structure containing the parameters required by the system

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

p.dimension = 3;
p.rank = 5; % rank used in CD decomposition  

p.filters_sep_directory = 'filters_sep'; % directory where the components of the separable filters are saved in a txt format
p.filters_txt_directory = 'filters_txt'; % directory where the whole separable filters are saved in a txt format (N.B. the whole filter is saved, not only the separable components )
p.filters_img_directory = 'filters_img'; % directory where the separable filters are saved in a nrrd format
p.reconstructed_filters_txt_directory = 'reconstructed_filters_txt';  % directory where the reconstruction of the original filters is saved in a txt format
p.reconstructed_filters_img_directory = 'reconstructed_filters_img'; % directory where the reconstruction of the original filters is saved in a nrrd format

p.coeffs_txt_directory = 'coeffs_rec_txt'; % directory where the coefficients to compute the reconstruction starting from the separable fiters are saved

p.stats_directory = 'stats'; % directory where statistics about the optimization are saved


%create directories
if(resume_fb == 0)
    [status, message, messageid] = rmdir(p.filters_sep_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.filters_txt_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.filters_img_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.reconstructed_filters_txt_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.reconstructed_filters_img_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.stats_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.coeffs_txt_directory, 's'); %#ok<*NASGU,*ASGLU>

    [status, message, messageid] = mkdir(p.filters_sep_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.filters_txt_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.filters_img_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.reconstructed_filters_txt_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.reconstructed_filters_img_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.stats_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.coeffs_txt_directory); %#ok<*ASGLU,*NASGU>

end

p.original_filters_filename = 'data_3D/NAME_OF_ORIGINAL_FILTER_BANK.txt'; %txt file containing the original filter bank

p.gradient_steps_no = 10; % number of gradient steps on coefficients
p.gradient_step_size = 1e-1; % gradient step size on coefficients
p.filters_grad_step_size = 7e-2; % gradient step size on sep filters
p.lambda_l1 = 0; % regularization parameter on the coefficients
p.lambda_nuclear = 1e-2;% regularizarion paramether on ktensor coefficients of the sep filters

p.filters_size = [11,11,11];
p.filters_no = 9; %number of separable filters used to approximate original filter bank

p.steps_no = 300; % number of steps after which partial results are saved

%parameters for visualization
p.vSpace = 4;
p.hSpace = 4;
p.pixS = 2;


end

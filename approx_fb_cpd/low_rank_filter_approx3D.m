function [] = low_rank_filter_approx3D(varargin)
% low_rank_filter_approx3D approximate a 3D filter bank with a bank of 3D separable filters
%using the algorith described in [1]
%
% []=low_rank_filter_approx3D(resume_fb_numer)
%   if resume_fb_numer>0 restarts a previous simulation stopped at iteration resume_fb_numer 
%
% requires: tensor and poblano toolboxes :
% http://www.sandia.gov/~tgkolda/TensorToolbox/ and uses cp_opt algorithm
% of [2]
%
% [1] Learning separable filters
% [2] E. Acar, D. M. Dunlavy and T. G. Kolda. A Scalable Optimization Approach for Fitting Canonical Tensor Decompositions, Journal of Chemometrics 25(2):67-86, February 2011
% 

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

addpath(genpath('helper_scripts_3D'));

numvarargs = length(varargin);
if(numvarargs > 1)
    error('Too many inputs, the only supplemental parameter allowed is, eventually, the fb number to resume');
else
    if(numvarargs == 1)
        resume_fb_number = varargin{1};
    else
        resume_fb_number = 0;
    end
end

p = get_config(resume_fb_number); % <-- SET PARAMETERS HERE
[p, original_filters] = load_original_fb(p);
[p,M] = init_filters(p, resume_fb_number);


count = resume_fb_number*p.steps_no+1;
sampling_prob = ones(p.original_fb_no,1);


while(true) % type Ctrl-C to stop 

    rand_filter_n = randsample(p.original_fb_no,1,true,sampling_prob);
    x = original_filters(:, :, :,rand_filter_n); 
    x = x(:);
    
    t = optimize_coeffs(p,M,x);
    
    % Optimize filters
    res = x-M*t;
    M_grad = -2*res*t'*p.filters_grad_step_size;
    M = M-M_grad;
    

    %compute CP decomposition and make soft thresholding on ktensor coefficients
    [p,M] = apply_componentwise_NN_proximal_operator3D(p,M); 
    
    
    % Re-evaluate probabilities
     if(rem(count,50) == 0)
         fprintf('  Iteration %d, evaluating sampling probabilities\n',count);
         [sampling_prob] = eval_sampling_prob(p,M,original_filters);
     end
     
     %save 
     if(rem(count, p.steps_no) == 0)
         fprintf('  Storing data on disk\n');
         nrrdSave(sprintf('%s/fb_%06d.nrrd', p.filters_img_directory, floor(count/p.steps_no)), permute(reshape_fb_as_img(p, M),[2,1,3])); %

         save_filter_bank(p, M, count);
         save_reconstructed_filter_bank(p, M, count, original_filters);
     end
     
    %normalize after saving to preserve saved CP decomposition
    M = normalize_fb(p,M);
     
    count = count + 1;
end


end

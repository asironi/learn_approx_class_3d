function [] = save_filter_bank3D(p,fb,it_count)
%  save_filter_bank  saves the filter bank both as a text file and as an
%                    image
%
%  Synopsis:
%     save_filter_bank(p,fb,it_count)
%
%  Input:
%     p        = structure containing the parameters required by the system
%     fb       = cell array containing the filters
%     it_count = iteration counter

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

% Save the filter bank in a text file
fd = fopen(sprintf(p.paths.fb_txt_fileformat,it_count/p.iterations_no),'wt');
for i_filter = 1:p.filters_no
    % Dump the filters a row at a time
    for s = 1:p.filters_size(3)
        for r = 1:p.filters_size(1)
            fprintf(fd,'%f ',fb{i_filter}(r,:,s));
            fprintf(fd,'\n');
        end
    end
end
fclose(fd);

% Save the filter bank as an image
reshape_fb_as_image3D(sprintf(p.paths.fb_img_fileformat,it_count/p.iterations_no),sprintf(p.paths.fb_txt_fileformat,it_count/p.iterations_no),p.filters_no,p.filters_size,p.pixel_size,[p.v_space,p.h_space]);


end

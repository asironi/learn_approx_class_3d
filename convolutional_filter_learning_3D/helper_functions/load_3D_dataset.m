function [dataset] = load_3D_dataset(p)
%  load_3D_dataset  loads dataset,
%
%  Synopsis:
%     [dataset] = load_3D_dataset(p)
%
%  Input:
%     p = structure containing framework's configuration
%  Output:
%     dataset = loaded dataset

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

    [images_list,images_no] = get_list(p.dataset_filelist);

    dataset = cell(images_no,1);
    
    % Load and convert the images to grayscale, storing them
    % in the dataset's cell array
    fprintf('  Loading dataset\n');

    min_rescale = -0.1;
    max_rescale = 0.1;
          
    for i_img = 1:length(dataset),
        
        dataset{i_img} = im2double(nrrdLoad(images_list{i_img}));    
        dataset{i_img} = permute(dataset{i_img},[2,1,3]);
        
        % rescale in [-0.1,0.1]
        max_img = max(dataset{i_img}(:) );
        min_img = min(dataset{i_img}(:) );
        
        if (max_img-min_img>eps)
            dataset{i_img}=(max_rescale-min_rescale)*(dataset{i_img}-min_img)/(max_img-min_img)+min_rescale;
        end
        
    end

end

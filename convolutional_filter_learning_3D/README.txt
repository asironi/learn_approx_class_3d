3D convolutional filter learning - MATLAB version

This code implements a 3D extension of the algorithm presented in [1].

The algorithm's settings are in the helper_functions/get_config.m file.

- Required libraries: Teem/nrrd library ([2]). Once compiled for your
  architecture, copy them in the helper_functions/nrrd/ subdirectory.

If you use this code in your project, please cite our paper ([3]).

For any question or bug report, please feel free to contact me at:
amos <dot> sironi <at> epfl <dot> ch


[1] R. Rigamonti, M. Brown, V. Lepetit, "Are Sparse Representations Really
    Relevant for Image Classification?", CVPR 2011
[2] http://teem.sourceforge.net/nrrd/index.html
[3] R. Rigamonti, A. Sironi, V. Lepetit and P. Fua. Learning Separable Filters. IEEE International Conference on Computer Vision and Pattern Recognition (CVPR 2013), Portland, Oregon, USA, 2013.
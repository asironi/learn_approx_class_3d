------- Results Folder -------

Folder where the results will be saved: 
- the filter bank will be saved with .nrrd extension in a folder called filters_img
- the filter bank will be saved in a .txt file in folder called filters_txt. 
If N is the number of learned filters and R,C,S are respectively the number rows, columns and slices of the filters, then the .txt file will have C columns and R*S*N rows, where the first R*S rows correspond to the first filter (first row corresponding to first row of the first slice, second row to the second row of the first slice, … , R-th row to the last row of the first slice, …, R*S-th row corresponding to the last row of the last slice), rows from R*S+1 to 2*R*S correspond to the second filter and so on.

The name of the folder where you want to save the results has to be given in get_gonfig_3D.m as value of variable p.results_path and should be an existing folder. 
N.B. If in the result's folder are saved results corresponding to a previous optimization, they will be deleted when running a new optimization with the same result's folder as a parameter. 


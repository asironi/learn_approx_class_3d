function [] = pixel_classification_3D(fb_name,use_oof,use_ef,classifier)
%  pixel_classification_3D  Feature extraction and classification steps
%                        required to perform the pixel classification task
%                        on medical images. For more details please refer to
%                        [1] and [2].
%
%  Synopsis:
%     pixel_classification_3D_final(fb_name,use_oof,use_ef,classifier)
%
%  Input:
%     fb_name    = name of the filter bank used for feature extraction
%     use_oof    = flag used to enable the use of OOF in classification
%     use_ef     = flag used to enable the use of EF in classification
%     classifier = name of the classifier requested by the user
%
%  [1] R. Rigamonti and V. Lepetit, "Accurate and Efficient Linear Structure
%      Segmentation by Leveraging Ad Hoc Features with Learned Filters", MICCAI
%      2012
%  [2] Learning Separable Filters
%

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

addpath(genpath('helper_functions_3D')); 
addpath(genpath('libs_3D')); 

[p] = get_config_classification(fb_name,use_oof,use_ef,classifier); % <--- MODIFY HERE the algorithm's parameters

[p.filter_bank] = load_filter_bank(p);

% Training images
fprintf('    compute feature maps for TRAINING images\n');
create_train_list(p);
p.sep_comb_star = false; % always train will all features
for i_train_img = 1:p.train_imgs_no
    compute_feature_maps_train(p,i_train_img);
end

% Test images
fprintf('    compute feature maps for TEST images\n');
create_test_list(p);    
if(~isempty(p.weight_matrix_path) && p.separable_filters_flag && strcmp(p.classifier, 'l1reg')) % TODO: SEP_COMB* with RF classifier
    p.sep_comb_star = true; % for test use only features of separable filters 
end
for i_test_img = 1:p.test_imgs_no
    compute_feature_maps_test(p,i_test_img);
end

fprintf('  Getting test samples\n');
[test_data_filenames,test_labels_filenames] = get_test_samples(p); %#ok<NASGU>

fprintf('  Creating train batches, training, and testing\n');
for i_rep = 1:p.test_repetitions_no
    % Get the same training samples for a given repetition number, to have a
    % fair comparison of the filter banks
    stream0 = RandStream('mt19937ar','Seed',i_rep);
    RandStream.setDefaultStream(stream0); %#ok<*SETRS>
    
    [train_data_filename,train_labels_filename] = get_train_samples(p,i_rep);

    switch p.classifier
        case 'RF'
            model = train_RF(p,train_data_filename);
            test_RF(p.paths.responses,model,p.test_imgs_list,test_data_filenames,i_rep);
        case 'l1reg'
            train_l1reg(p,train_data_filename,train_labels_filename,i_rep);
            test_l1reg(p,test_data_filenames,i_rep);
         otherwise
            error('%s: unknown classifier requested',p.classifier);
    end
end

%% Compute ROC, PR, and analytical measures
fprintf('  Computing ROC, PR, and analytical measures\n');
compute_performances(p);

end
3D Pixel Classification framework for medical images

This code extend [1] to 3D.

This code implements the framework presented in [2] and [3], which learns a filter bank
on datasets of medical images and then segments them.

Optionally you can use the output of two handcrafted algorithms:
- Optimally Oriented Flux (OOF)
- Enhancement Filtering (EF)
to support the algorithm.

The algorithm's settings are in the helper_functions_3D/get_config_classification.m file.

Details about the two components of the framework are given in the respective
subdirectories.

For any question or bug report, please feel free to contact me at:
amos <dot> sironi <at> epfl <dot> ch

If you use this code in your project, please cite our paper ([3]).

[1] https://bitbucket.org/roberto_rigamonti/med_img_pc
[2] R. Rigamonti and V. Lepetit, "Accurate and Efficient Linear Structure
    Segmentation by Leveraging Ad Hoc Features with Learned Filters", MICCAI
    2012
[3] R. Rigamonti, A. Sironi, V. Lepetit and P. Fua. Learning Separable Filters. IEEE International Conference on Computer Vision and Pattern Recognition (CVPR 2013), Portland, Oregon, USA, 2013.

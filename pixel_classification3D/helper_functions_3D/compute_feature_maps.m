function [fm_count] = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count)
%  compute_feature_maps  Extracts the feature maps from a given image using the
%                        specified filter bank
%
%  Synopsis:
%     [fm_count] = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count)
%
%  Input:
%     p              = structure containing system's configuration and paths
%     img            = source image for feature maps' computation
%     mask           = mask for the input image
%     fm_output_dir  = directory where the feature maps are stored
%     fm_file_format = file format for the saved feature maps
%     fm_count       = feature maps' count (used to generate the progressive
%                      filename)
%  Output:
%     fm_count = incremental counter for the feature maps

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

fb = p.filter_bank.fb;
filters_no = p.filter_bank.no;
filters_size = p.filter_bank.size;
weights = p.filter_bank.weights;


% Normalize the image before filtering
img = normalize_in_mask(img,mask);
size_img = size(img);

%compute quantities for convolution
size_half_kernel = ceil((filters_size-1)/2);
size_img_extended = size_img+2*size_half_kernel;


if(~isempty(weights) && ~(p.sep_comb_star))
    sep_conv_mat = zeros(prod(size_img), size(fb,2));
end

if (p.separable_filters_flag) % separable filters convolution
    
    central_part_fm_x = size_half_kernel(1)+(1:size_img(1));
    central_part_fm_y = size_half_kernel(2)+(1:size_img(2));
    central_part_fm_z = size_half_kernel(3)+(1:size_img(3));
    
    sep_filters_no = size(fb,2);
    
    %extract separable components : filter_i = A(:,i)oB(:,i)oC(:,i)
    A = fb(1:filters_size(1),:);
    B = fb(filters_size(1)+1:filters_size(1)+filters_size(2),:);
    C = fb(filters_size(1)+filters_size(2)+1:filters_size(1)+filters_size(2)+filters_size(3),:);
    
    
  for i_fm = 1:sep_filters_no, % number of separable filters
    % no boundary conditions
    %fm_temp = conv2(reshape(img,size_img(1),size_img(2)*size_img(3)), A(end:-1:1,i_fm),'same'); %convolution in x
    %fm_temp = conv2(reshape(fm_temp.',size_img(2),size_img(1)*size_img(3)), B(end:-1:1,i_fm),'same'); %convolution in y
    %fm_temp = conv2(reshape(fm_temp.',size_img(3),size_img(1)*size_img(2)), C(end:-1:1,i_fm),'same'); %convolution in z
    %fm_temp= reshape(fm_temp.',size_img);
     
    %symmetric boundary conditions
    fm_temp = conv2(reshape(padarray(img,size_half_kernel,'symmetric','both'),size_img_extended(1),size_img_extended(2)*size_img_extended(3)), A(end:-1:1,i_fm),'same'); %convolution in x
    fm_temp = conv2(reshape(fm_temp.',size_img_extended(2),size_img_extended(1)*size_img_extended(3)), B(end:-1:1,i_fm),'same'); %convolution in y
    fm_temp = conv2(reshape(fm_temp.',size_img_extended(3),size_img_extended(1)*size_img_extended(2)), C(end:-1:1,i_fm),'same'); %convolution in z
    fm_temp= reshape(fm_temp.',size_img_extended);
    fm_temp=fm_temp(central_part_fm_x,central_part_fm_y,central_part_fm_z);
         
    
    if(isempty(weights) || p.sep_comb_star) % save fm of separable filters
        feature_map = fm_temp;
        
        % Dump response to file
        nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),permute(feature_map,[2,1,3]));

        fm_count = fm_count+1;
    else 
        sep_conv_mat(:,i_fm) = fm_temp(:);
    end
    
  end
  
  if(~isempty(weights) && ~(p.sep_comb_star)) % compute fm correspondig to whole filter bank from separable ones
     feature_maps_matrix = sep_conv_mat*weights; 
     feature_maps_matrix = reshape(feature_maps_matrix,[size_img,filters_no]);
     for i_fm = 1:filters_no
        feature_map =  feature_maps_matrix(:,:,:,i_fm);
        % Dump response to file
        nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),permute(feature_map,[2,1,3]));
        fm_count = fm_count+1;
     end
  end
  
else %fft based convolution
    
    central_part_fm_x = filters_size(1)+1*(~rem(filters_size(1),2)):size_img_extended(1);
    central_part_fm_y = filters_size(2)+1*(~rem(filters_size(2),2)):size_img_extended(2);
    central_part_fm_z = filters_size(3)+1*(~rem(filters_size(3),2)):size_img_extended(3);
    
    %size for fourier transform of filters
    size_fft_extended = size_img_extended+filters_size-1;
                     
    %fft of image (symmetric boundary conditions)
    fft_img = fftn(padarray(img,size_half_kernel,'symmetric','both'),size_fft_extended);
    %fft of image (no boundary conditions)
    %fft_img = fftn(padarray(img,size_half_kernel,0,'both'),size_fft_extended);
    %clear img
                      
    fft_fb = cell(filters_no,1);
    for i_filter = 1:filters_no,
        fft_fb{i_filter} = fftn(fb{i_filter}(end:-1:1,end:-1:1,end:-1:1),size_fft_extended); %fft of flipped filter to compute correlation
    end

    for i_fm = 1:filters_no,
        %convolution in frequency domain
        fft_fm= fft_img.*fft_fb{i_fm};

        %back to spatial domain and extract central part
        feature_map = ifftn(fft_fm);
        feature_map = feature_map(central_part_fm_x,central_part_fm_y,central_part_fm_z);

        % Dump response to file
        nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),permute(feature_map,[2,1,3]));

        fm_count = fm_count+1;
           
    end
end

end

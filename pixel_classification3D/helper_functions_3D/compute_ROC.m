function [] = compute_ROC(ROC_dir_rep,tp,fp,tn,fn,test_img_name,fpr_eval_grid)
%  compute_ROC  Compute Receiver Operating Characteristic curve for the given
%               image
%
%  Synopsis:
%     compute_ROC(ROC_dir_rep,response,gt,test_img_name,thresholds_no,fpr_eval_grid)
%
%  Input:
%     ROC_dir_rep   = directory where the ROC curve for the given image will be
%                     saved
%     response      = response file given by the classifier
%     gt            = ground-truth image
%     test_img_name = name of the test image
%     thresholds_no = number of thresholds used in ROC computation
%     fpr_eval_grid = grid over which the ROC will be computed

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

tpr = tp./(tp+fn);
fpr = fp./(fp+tn);
tpr(end) = 0;
tpr(1) = 1;

% Perform spline interpolation
tpr_fit = csapi(fpr,tpr);
tpr = fnval(tpr_fit,fpr_eval_grid);
tpr(tpr>1) = 1;
tpr(tpr<0) = 0;

% Compute Area Under Curve by integration
auc = -trapz(fpr_eval_grid(end:-1:1),tpr(end:-1:1));
%auc = -trapz(fpr(end:-1:1),tpr(end:-1:1));


ROC_filename = fullfile(ROC_dir_rep,sprintf('ROC_%s.txt',test_img_name));
AUC_filename = fullfile(ROC_dir_rep,sprintf('AUC_%s.txt',test_img_name));

fd = fopen(ROC_filename,'wt');
for i_pt = 1:length(fpr_eval_grid)
    fprintf(fd,'%f %f\n',fpr_eval_grid(i_pt),tpr(i_pt));
end
fclose(fd);

fd = fopen(AUC_filename,'wt');
fprintf(fd,'%f\n',auc);
fclose(fd);

end

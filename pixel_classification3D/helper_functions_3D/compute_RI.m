function [] = compute_RI(analytical_dir_rep,response,gt,test_img_name,best_threshold)
%  compute_RI  Compute the Rand Index measure for the given segmentation
%
%  Algorithm taken from the code published by Allen Y. Yang
%  http://www.eecs.berkeley.edu/~yang/software/lossy_segmentation/
%
%  Synopsis:
%     compute_RI(analytical_dir_rep,response,gt,test_img_name,best_threshold)
%
%  Input:
%     analytical_dir_rep = directory where the RI measure for the given
%                          segmentation will be saved
%     response           = response file given by the classifier
%     gt                 = ground-truth image
%     test_img_name      = name of the test image
%     best_threshold     = threshold for which the highest F-measure has been
%                          attained

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

% Threshold response at best threshold
response(response<best_threshold) = 0;
response(response>=best_threshold) = 1;
count_matrix = compute_count_matrix(response,gt);


N = sum(count_matrix(:));

n_u = sum(count_matrix,2); % Sum over cols
n_v = sum(count_matrix,1); % Sum over rows

N_choose_2 = N*(N-1)/2;

RI_index = 1-(sum(n_u.*n_u)/2+sum(n_v.*n_v)/2-sum(sum(count_matrix.*count_matrix)))/N_choose_2;

RI_filename = fullfile(analytical_dir_rep,sprintf('RI_%s.txt',test_img_name));
fd = fopen(RI_filename,'wt');
fprintf(fd,'%f\n',RI_index);
fclose(fd);

end

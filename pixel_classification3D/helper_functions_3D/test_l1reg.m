function [] = test_l1reg(p,test_filenames,i_rep)
%  test_l1reg  Test a trained l1-penalized regressor on the given test data
%
%  Synopsis:
%     test_l1reg(results_path,model,test_imgs_list,test_filenames,i_rep)
%
%  Input:
%     p                  = structure containing system's configuration and paths 
%     test_data_filename = name of the file containing the test data
%     i_rep              = iteration number (for multiple random tests)

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

fprintf('  Testing l1-penalized logistic regressor on input data...\n');

results_path = p.paths.responses;
test_imgs_list = p.test_imgs_list;
%load model learned from trainng
model_filename = fullfile(p.paths.var,sprintf('l1_reg_model_rep%03d',i_rep));

if (~exist(p.l1_test_path,'file'))
    error('Wrong path for the l1 classifier (%s)',p.l1_test_path);
end

[status,message,messageid] = mkdir(fullfile(results_path,sprintf('rep_%03d',i_rep))); %#ok<ASGLU>
[status,message,messageid] = mkdir(fullfile(p.paths.res,'predictions',sprintf('rep_%03d',i_rep))); %#ok<ASGLU>


for i_test_file = 1:length(test_filenames)
    [test_img_path,test_img_name,test_img_ext] = fileparts(test_imgs_list{i_test_file}); %#ok<*NASGU,ASGLU>
    
    %path where to save predictions of the model
    pred_filename = fullfile(p.paths.res,'predictions',sprintf('rep_%03d',i_rep),sprintf('%s',test_img_name));
    
    if(~(p.sep_comb_star)) % use all feature maps 
        %apply model to test img
        system(sprintf('%s -p %s %s %s',p.l1_test_path,model_filename,test_filenames{i_test_file},pred_filename));
        pred = mmread(pred_filename);
    else % use only fm of separable filters and deduce linear model from the one trained with all filters
        
        %load model 
        model = mmread(model_filename);
        intercept = model(1);
        coeffs = model(2:end);
        %get weights
        weights = p.filter_bank.weights;
        %compute new model
        new_coeffs = weights*coeffs;
        new_model = [intercept;new_coeffs];
        %write new model
        new_model_filename = fullfile(p.paths.var,sprintf('l1_reg_model_star_rep%03d',i_rep));
        mmwrite(new_model_filename,new_model);
        %apply new model to test img
        system(sprintf('%s -p %s %s %s',p.l1_test_path,new_model_filename,test_filenames{i_test_file},pred_filename));
        pred = mmread(pred_filename);
    end
    result_filename = fullfile(results_path,sprintf('rep_%03d',i_rep),sprintf('%s.nrrd',test_img_name));
    test_img = nrrdLoad(test_imgs_list{i_test_file});
    
    % Reshape prediction to the same format of input image
    size_test_img = size(test_img);
    output = reshape(pred, size_test_img);
    
    %test_img = permute(test_img,[2,1,3]); 
    %output = zeros(size_test_img);
    %for i_row = 1:size_test_img(1)
    %    for i_col = 1:size_test_img(2)
    %        for i_sli = 1:size_test_img(3)
    %           output(i_row,i_col,i_sli) = pred(size_test_img(2)*(size_test_img(1)*(i_sli-1) + i_row-1 ) + i_col);
    %        end
    %    end
    %end
    nrrdSave(result_filename,output); 

end

end

function [fm_count,fully_computed] = check_and_link(p,fm_output_dir,fm_file_format,fm_count,fully_computed,current_str,old_str)

if (~isempty(strfind(fm_output_dir,current_str)))
    fully_computed = true;
    for i_fm = 0:p.filter_bank.no-1
        if (~exist(sprintf(fm_file_format,regexprep(fm_output_dir,current_str,old_str),i_fm),'file'))
            fully_computed = false;
            break;
        end
    end
    if (fully_computed)
        fprintf('    An version of the feature maps with %s has been previously computed, loading it instead of re-doing the computations...\n',old_str);
        for i_fm = 0:p.filter_bank.no-1
            system(sprintf('ln -s %s %s',sprintf(fm_file_format,regexprep(fm_output_dir,current_str,old_str),i_fm),sprintf(fm_file_format,fm_output_dir,i_fm)));
            fm_count = p.filter_bank.no;
        end
    end
end

end

#include <vector>
#include <string>
#include "mex.h"
#include <string.h>
#include <algorithm>

#define fatalMsg(x) mexErrMsgTxt(x)

#include "include/matlab_utils.hxx"
//#include "utils.h"

// otherwise it performs a 'smart' algorithm to reduce
//  output size
#define INCLUDE_ALL_POINTS 0

using namespace std;

template<typename T>
struct ScoreLabel
{
    T score;
    unsigned char label;

    inline bool operator< (const ScoreLabel<T>& b) const
    {
        return score < b.score;
    }

    inline bool isInside( T thr ) {
        return score >= thr;
    }
};


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if (nrhs != 2)
      mexErrMsgTxt("Incorrect input format: [TP FP TN FN Thr]  = roc_pr_float( scoreImg, labelImg )\n");

  if (nlhs != 5) {
      mexErrMsgTxt("Four output args required.");
  }

#define mScores (prhs[0])
#define mLabels (prhs[1])

  typedef float ScoreType;
  typedef unsigned char MainType;

  if (mxGetClassID(mScores) != matlabClassID<ScoreType>())
      mexErrMsgTxt("Scores must be SINGLE class");

  if (mxGetClassID(mLabels) != matlabClassID<MainType>())
      mexErrMsgTxt("Labels must be UINT8 class");

  if ( (mxGetNumberOfDimensions(mScores) != 2) || (mxGetN(mScores) != 1) )
      mexErrMsgTxt("Scores must be 1-dimensional");

  if ( (mxGetNumberOfDimensions(mLabels) != 2) || (mxGetN(mLabels) != 1) )
      mexErrMsgTxt("Labels must be 1-dimensional");

  const unsigned int N = mxGetM(mScores);
  if (N != mxGetM(mLabels))
      mexErrMsgTxt("Scores and labels must be of same size");


  // create score and sort
  std::vector< ScoreLabel<ScoreType> >  scoreLabels( N );
  {
      const MainType *lblPtr = (const MainType *) mxGetData(mLabels);
      const ScoreType *scPtr = (const ScoreType *) mxGetData(mScores);

      for (unsigned i=0; i < N; i++) {
          scoreLabels[i].label = lblPtr[i];
          scoreLabels[i].score = scPtr[i];
      }

      std::sort( scoreLabels.begin(), scoreLabels.end() );
  }

  ScoreType thr = scoreLabels[0].score;

  std::vector<double>   vTP, vTN, vFP, vFN;
  std::vector<ScoreType> vThr;

  // check how do we start
  unsigned int TP = 0;
  unsigned int FP = 0;
  unsigned int TN = 0;
  unsigned int FN = 0;

  for (unsigned i=0; i < N; i++)
  {
      bool yes = scoreLabels[i].isInside(thr);
      if (yes && scoreLabels[i].label)
          TP++;
      else if( yes && (!scoreLabels[i].label) )
          FP++;
      else if ( (!yes) && (!scoreLabels[i].label) )
          TN++;
      else
          FN++;
  }

  MainType lastLabel = 23;  // not matching any label
  
  for (unsigned i=0; i < N; i++)
  {
      // check if label has changed, then save values
      #if !INCLUDE_ALL_POINTS
      if ( (lastLabel != scoreLabels[i].label) && (thr != scoreLabels[i].score))
      #else
      if ( thr != scoreLabels[i].score )
      #endif
      {
          vTP.push_back( TP );
          vFN.push_back( FN );
          vFP.push_back( FP );
          vTN.push_back( TN );

          thr = scoreLabels[i].score;
          lastLabel = scoreLabels[i].label;

          vThr.push_back(thr);
      }

      if (scoreLabels[i].label)
      {
          TP--;
          FN++;
      } else
      {
          TN++;
          FP--;
      }
  }


  // now save to matlab
  MatlabOutputMatrix< double >  pTP( &plhs[0], vTP.size(), 1 );
  MatlabOutputMatrix< double >  pFP( &plhs[1], vTP.size(), 1 );
  MatlabOutputMatrix< double >  pTN( &plhs[2], vTP.size(), 1 );
  MatlabOutputMatrix< double >  pFN( &plhs[3], vTP.size(), 1 );
  MatlabOutputMatrix< ScoreType >  pThr( &plhs[4], vTP.size(), 1 );

  memcpy( pTP.data(), vTP.data(), sizeof(double) * vTP.size() );
  memcpy( pFN.data(), vFN.data(), sizeof(double) * vFN.size() );
  memcpy( pFP.data(), vFP.data(), sizeof(double) * vFP.size() );
  memcpy( pTN.data(), vTN.data(), sizeof(double) * vTN.size() );

  memcpy( pThr.data(), vThr.data(), sizeof(ScoreType) * vThr.size() );

  mexPrintf("Done\n");
  mexEvalString("drawnow");
}

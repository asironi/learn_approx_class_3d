function [train_data_filename,train_labels_filename] = get_train_samples(p,i_rep)
%  get_train_samples  Convert the feature maps in a format suitable for the
%                     classifiers; take N random samples, N positive samples
%                     and N/2 samples aroud ground truth
%
%  Synopsis:
%     [train_data_filename,train_labels_filename] = get_train_samples(p)
%
%  Input:
%     p     = structure containing system's configuration and paths
%     i_rep = iteration number (for multiple random tests)
%  Output:
%     train_data_filename   = file containing the training data for the given
%                             round 
%     train_labels_filename = file containing the training labels for the given
%                             round 

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

[train_dirs_list,train_dirs_no] = get_list(p.paths.train_fm_list);
 
train_data_filename = fullfile(p.paths.train_data,sprintf('train_round_%d.txt',i_rep));
train_labels_filename = fullfile(p.paths.train_labels,sprintf('train_round_%d.txt',i_rep));

samples_collected_no = 0;
N_pos_tot = 0;
N_neg_tot = 0;

pos_samples_tot = cell(train_dirs_no,1);
neg_samples_tot = cell(train_dirs_no,1);

for i_dir = 1:train_dirs_no
    % Number of samples to collect on this image
    if (i_dir<train_dirs_no)
        N = floor(p.train_samples_no/train_dirs_no);
    else
        N = p.train_samples_no-samples_collected_no;
    end
    samples_collected_no = samples_collected_no+N;

    % Load pos/neg sampling areas and the corresponding fms
    %Load gt
    gt = im2double(nrrdLoad(p.pos_sampling_list{i_dir}));
    gt = permute(gt,[2,1,3]);

     
     pos_mask = im2double(nrrdLoad(p.pos_sampling_list{i_dir}));
     pos_mask = permute(pos_mask,[2,1,3]);
     pos_idx = find(pos_mask~=0);

     % to take negative samples
     %neg_mask = im2double(nrrdLoad(p.neg_sampling_list{i_dir}));
     %neg_mask = permute(neg_mask,[2,1,3]);
     %neg_idx = find(neg_mask~=0);
 
    
     %difficult samples to classify are those around the gt
    dilat_mask = im2double(nrrdLoad(p.dilat_sampling_list{i_dir}));
    dilat_mask = permute(dilat_mask,[2,1,3]);
    dilat_idx = find(dilat_mask~=0);

    %to be sure to have positive  samples
    pos_swapping = randi(length(pos_idx),N,1);
    %neg_swapping = randi(length(neg_idx),floor(N/2),1);
    dilat_swapping = randi(length(dilat_idx),floor(N/2),1);
    
    pos_samples_coord_1 = pos_idx(pos_swapping);                                          
    dilat_samples_coord = dilat_idx(dilat_swapping);
    %neg_samples_coord_1 = neg_idx(neg_swapping);
    
    pos_dilat_index = gt(dilat_samples_coord)>0;
    pos_samples_coord_dilat = dilat_samples_coord(pos_dilat_index);
    neg_samples_coord_dilat = dilat_samples_coord(~pos_dilat_index);
    

    
    %uniform sampling
    rand_index = randi(length(gt(:)),N,1);    
    pos_index = gt(rand_index)>0;    
    pos_samples_coord = rand_index(pos_index);
    neg_samples_coord = rand_index(~pos_index);
    N_pos = length(pos_samples_coord)+length(pos_samples_coord_dilat)+length(pos_samples_coord_1);
    N_neg = length(neg_samples_coord)+length(neg_samples_coord_dilat);
    
    
    % Load feature maps
    fm_dir = train_dirs_list{i_dir};
    fm_list = dir(fullfile(fm_dir,'*.nrrd'));
    fm_no = length(fm_list);

    pos_samples_tot{i_dir} = zeros(N_pos,fm_no);
    neg_samples_tot{i_dir} = zeros(N_neg,fm_no);

    
    for i_fm = 1:fm_no
        fm = nrrdLoad(fullfile(fm_dir,fm_list(i_fm).name));
        fm = permute(fm,[2,1,3]);
      
        if (sum((isnan(fm(:))))>0)
            error('NaN: train dir %d, fm %d\n',i_dir,i_fm);
        end 

         pos_samples_tot{i_dir}(:,i_fm) = fm([pos_samples_coord;pos_samples_coord_dilat;pos_samples_coord_1]);
         neg_samples_tot{i_dir}(:,i_fm) = fm([neg_samples_coord;neg_samples_coord_dilat]);        
 
    end
    clear fm
    
    N_pos_tot = N_pos_tot+N_pos;
    N_neg_tot = N_neg_tot+N_neg;
        

end

pos_samples_tot = cell2mat(pos_samples_tot);
neg_samples_tot = cell2mat(neg_samples_tot);

switch p.classifier
        case 'RF'
            samples = [pos_samples_tot, ones(N_pos_tot,1);neg_samples_tot, zeros(N_neg_tot,1)];
            clear pos_samples_tot neg_samples_tot
            
            dlmwrite(train_data_filename,samples,'delimiter','\t')
            dlmwrite(train_labels_filename,[ones(N_pos_tot,1);zeros(N_neg_tot,1)]);
        case 'l1reg'
            mmwrite(train_labels_filename,[ones(N_pos_tot,1);-ones(N_neg_tot,1)]);
            mmwrite(train_data_filename,[pos_samples_tot;neg_samples_tot]); 
end

end
function [] = create_train_list(p)
%  create_train_list  Create a file containing the list of files corresponding
%                     to training feature maps
%
%  Synopsis:
%     create_train_list(p)
%
%  Input:
%     p = name of the dataset used for training

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

fd = fopen(p.paths.train_fm_list,'wt');

for i_train_img = 1:p.train_imgs_no
    img_filename = p.train_imgs_list{i_train_img};
    [img_path,img_name,img_ext] = fileparts(img_filename); %#ok<*NASGU,ASGLU>
    fm_output_dir = fullfile(p.paths.train_fm_dir,img_name);
    fprintf(fd,'%s\n',fm_output_dir);
end
fclose(fd);

end

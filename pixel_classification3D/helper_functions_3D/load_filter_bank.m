function [filter_bank] = load_filter_bank(p)
%  load_filter_bank  Load a filter bank from file
%
%  Synopsis:
%     [filter_bank] = load_filter_bank(p)
%
%  Input:
%     p = structure containing system's configuration and paths
%  Output:
%     filter_bank = structure containing a filter bank along with its size and
%                   the number of filters that compose it

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

if (isempty(p.fb_name))
   error('empty filter bank') 
end

fb_file = load(p.paths.fb);
    

if(p.separable_filters_flag)
    %get filters size
    if(isempty(p.filters_size)) % assumes cubic filters
        filters_size = ones(1,p.dimension)*(size(fb_file,1))/3; 
        if( filters_size~=round(filters_size)) 
            error('The specified filter bank (%s) has non-square filters: specify filters size',p.paths.fb);
        end
    else
        if (sum(p.filters_size) ~= size(fb_file,1))
            error('number of rows of filter bank must be equal to sum(p.filters_size)' );
        end
        filters_size = p.filters_size;
    end
    
    filter_bank.size = filters_size;
    
    if(~isempty(p.weight_matrix_path) ) % separable filter bank used to reconstruct a full rank filter bank
       filter_bank.weights = load(p.weight_matrix_path);
       filter_bank.no = size(filter_bank.weights,2); %
    else % consider only separable filters
       filter_bank.weights = [];
       filter_bank.no = size(fb_file,2); %
    end
    
    filter_bank.fb = fb_file;
    filter_bank.sep_filter_no = size(fb_file,2);
    
else
    %get filters size
    if(isempty(p.filters_size)) % assumes cubic filters
        filters_size = ones(1,p.dimension)*size(fb_file,2); 
        if( rem(size(fb_file,1),filters_size(1)^(p.dimension-1))~=0) 
            error('The specified filter bank (%s) has non-square filters: specify filters size',p.paths.fb);
        end
    else
        if (p.filters_size(2) ~= size(fb_file,2))
            error('number of columns of  filter bank must be equal to p.filters_size(2)' );
        end
        filters_size = p.filters_size;
    end

    filter_bank.no = size(fb_file,1)/(filters_size(1)*filters_size(3));%  must be integer
    if (filter_bank.no~=round(filter_bank.no))
        error('number of rows of filter bank must be equal to p.filters_size(1)*p.filters_size(1)*filters_no' );
    end

    filter_bank.size = filters_size;

    filter_bank.fb = cell(filter_bank.no,1);
    for i_filter = 1:filter_bank.no 
        filter_bank.fb{i_filter} = fb_file((i_filter-1)*filters_size(1)*filters_size(3)+1:i_filter*filters_size(1)*filters_size(3), :);
        filter_bank.fb{i_filter} = permute(reshape(filter_bank.fb{i_filter}',[filters_size(2),filters_size(1),filters_size(3)]),[2,1,3]);
    end
    
    filter_bank.weights = [];
    filter_bank.sep_filter_no = 0;
end
                                      
end
#include <stdio.h>
#include <string.h>
#include "math.h"
#include "mex.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  const mwSize *pSize = mxGetDimensions(prhs[0]);
  mwSize nDims = mxGetNumberOfDimensions(prhs[0]);
  double *pData = mxGetPr(prhs[0]);

  const mwSize *pSizeGT = mxGetDimensions(prhs[1]);
  mwSize nDimsGT = mxGetNumberOfDimensions(prhs[1]);
  double *pGT = mxGetPr(prhs[1]);

  int outFilenameLength = mxGetN(prhs[2])+1;
  char *outFilename = (char *)mxCalloc(outFilenameLength,sizeof(char));
  mxGetString(prhs[2],outFilename,outFilenameLength);

  int outFormatLength = mxGetN(prhs[3])+1;
  char *outFormat = (char *)mxCalloc(outFormatLength,sizeof(char));
  mxGetString(prhs[3],outFormat,outFormatLength);

  FILE *fd = fopen(outFilename,"wt");
  if (fd==NULL) {
    printf("Error encountered while opening file %s -- quitting\n",outFilename);
    return;
  }

  if (strcmp(outFormat,"l1reg")==0) {
    fprintf(fd,"%%%%MatrixMarket matrix array real general\n%d %d\n",pSize[0]*pSize[1]*pSize[2],pSize[3]);
    for (unsigned int s=0;s<pSize[3];++s) { //index for different feature maps
        for (unsigned int f =0;f<pSize[2];++f) {//run over fibers f // index of slices in a feature map
            for (unsigned int r=0;r<pSize[0];++r) {
                for (unsigned int c=0;c<pSize[1];++c) {
                    fprintf(fd,"%f\n",pData[r+c*pSize[0]+f*pSize[0]*pSize[1]+s*pSize[0]*pSize[1]*pSize[2]]);                }
            }
        }
    }
  }

  if (strcmp(outFormat,"RF")==0) {
    for (unsigned int r=0;r<pSize[0];++r) {
      for (unsigned int c=0;c<pSize[1];++c) {
          for (unsigned int f =0;f<pSize[2];++f) {//run over fibers f
              for (unsigned int s=0;s<pSize[3];++s) {
                  fprintf(fd,"%f, ",pData[r+c*pSize[0]+f*pSize[0]*pSize[1]+s*pSize[0]*pSize[1]*pSize[2]]); 
              }
              fprintf(fd,"%d\n",(int)pGT[r+c*pSize[0]+f*pSize[0]*pSize[1]]);
          }
      }
    }
  }


  fclose(fd);
  return;
}

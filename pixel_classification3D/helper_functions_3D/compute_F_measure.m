function [F_measure] = compute_F_measure(P,R)
%  compute_F_measure  Compute the F-measure for a given P/R value
%
%  Synopsis:
%     [F_measure] = compute_F_measure(P,R)
%
%  Input:
%     P = precision value
%     R = recall value
%  Output:
%     F_measure = F-measure value for the given P/R point

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

F_measure = 2*P*R/(P+R);

end

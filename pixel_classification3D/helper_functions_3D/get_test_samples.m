function [test_data_files,test_labels_files] = get_test_samples(p)
%  get_test_samples  Convert the feature maps in a format suitable for the
%                    classifiers
%
%  Synopsis:
%     [test_data_files,test_labels_files] = get_test_samples(p)
%
%  Input:
%     p = structure containing system's configuration and paths
%  Output:
%     test_data_files   = list of files containing the test data
%     test_labels_files = list of files containing the test labels

%  authors: Amos Sironi, Roberto Rigamonti, CVLab EPFL
%  e-mail: amos <dot> sironi <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~asironi
%  date: November 2012
%  last revision: 6 November 2012

% Get the list of directories containing the feature maps of test images
[test_dirs_list,test_dirs_no] = get_list(p.paths.test_fm_list);

test_data_files = cell(test_dirs_no,1);
test_labels_files = cell(test_dirs_no,1);

for i_test = 1:test_dirs_no

    % Get feature maps' list
    fm_dir = test_dirs_list{i_test};
    fm_list = dir(fullfile(fm_dir,'*.nrrd'));
    fm_no = length(fm_list);
    
    % Load the first feature map to get their size and allocate memory for loading them
    fm = permute(nrrdLoad(fullfile(fm_dir,fm_list(1).name)),[2,1,3]); 
    size_fm = size(fm);
    clear fm
    feature_maps = zeros([size_fm,fm_no]);
    
    % Initialize files for data/labels
     test_data_files{i_test} = sprintf('%s/test_%03d.txt',p.paths.test_data,i_test); %#ok<*PFBNS>
     test_labels_files{i_test} = sprintf('%s/test_%03d.txt',p.paths.test_labels,i_test);

     fd_labels = fopen(test_labels_files{i_test},'wt');


    % Load feature maps
    for i_fm = 1:fm_no
        feature_maps(:,:,:,i_fm) = permute(double(nrrdLoad(fullfile(fm_dir,fm_list(i_fm).name))),[2,1,3]); 
        
        if (sum(sum(isnan(feature_maps(:,:,i_fm))))>0)
            error('NaN: test dir %d, fm %d\n',i_test,i_fm);
        end 

    end
    
    % Load ground truth
    gt = double(nrrdLoad(p.test_gt_list{i_test})>0);
    gt = permute(gt,[2,1,3]);

    
    if(strcmp(p.classifier,'l1reg'))
        fprintf(fd_labels,'%%%%MatrixMarket matrix array real general\n%d %d\n',prod(size_fm),1);
    end
    
     % Dump data in the requested format
     switch p.classifier
         case 'RF'
            gt_per = permute(gt,[3,2,1]);
            fprintf(fd_labels,'%d\n',gt_per(:));
            clear gt_per
         case 'l1reg'
             % In this case, no labels in the data file
             gt(gt==0) = -1;
             gt = permute(gt,[2,1,3]);
             fprintf(fd_labels,'%d\n',gt(:));       
     end    

    write_test_file_3D(feature_maps,gt,test_data_files{i_test},p.classifier);
    fclose(fd_labels);

 end

 end

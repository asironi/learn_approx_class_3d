------- RF Folder -------

Put in this folder the files to test and train a Random Forrest classifier (see [1] and [2]).

[1] http://hci.iwr.uni-heidelberg.de/vigra/
[2] https://github.com/cbecker/vigra